#!/usr/bin/env python
# -*- coding: utf-8 -*-
###########################################################
#               WARNING: Generated code!                  #
#              **************************                 #
# Manual changes may get lost if file is generated again. #
# Only code inside the [MANUAL] tags will be kept.        #
###########################################################

from flexbe_core import Behavior, Autonomy, OperatableStateMachine, ConcurrencyContainer, PriorityContainer, Logger
from urbant_flexbe_states.check_drift_state import CheckDriftState
from urbant_flexbe_states.crossing_state import CrossingState
from urbant_flexbe_states.idle_state import IdleState
from urbant_flexbe_states.initialization_state import InitializationState
from urbant_flexbe_states.navigation_state import NavigationState
from urbant_flexbe_states.wait_for_crossing_state import WaitForCrossingState
# Additional imports can be added inside the following tags
# [MANUAL_IMPORT]

# [/MANUAL_IMPORT]


'''
Created on Thu Dec 09 2021
@author: Yu-Wen Chen
'''
class urbant_navgraph_behaviour_SM(Behavior):
	'''
	urbant_navgraph_behaviour
	'''


	def __init__(self):
		super(urbant_navgraph_behaviour_SM, self).__init__()
		self.name = 'urbant_navgraph_behaviour'

		# parameters of this behavior

		# references to used behaviors

		# Additional initialization code can be added inside the following tags
		# [MANUAL_INIT]
		
		# [/MANUAL_INIT]

		# Behavior comments:



	def create(self):
		# x:30 y:365, x:130 y:365
		_state_machine = OperatableStateMachine(outcomes=['finished', 'failed'])

		# Additional creation code can be added inside the following tags
		# [MANUAL_CREATE]
		
		# [/MANUAL_CREATE]

		# x:30 y:365, x:130 y:365, x:230 y:365, x:330 y:365, x:430 y:365, x:530 y:365, x:630 y:365, x:730 y:365, x:830 y:365, x:930 y:365, x:1030 y:365
		_sm_wait_for_crossing_0 = ConcurrencyContainer(outcomes=['crossing', 'idle', 'failed', 'drift', 'wait_for_crossing'], conditions=[
										('drift', [('check_drift_state_', 'drift')]),
										('failed', [('check_drift_state_', 'failed')]),
										('idle', [('Wait_for_crossing', 'idle')]),
										('crossing', [('Wait_for_crossing', 'crossing')]),
										('failed', [('Wait_for_crossing', 'failed')]),
										('wait_for_crossing', [('Wait_for_crossing', 'remain')])
										])

		with _sm_wait_for_crossing_0:
			# x:30 y:40
			OperatableStateMachine.add('Wait_for_crossing',
										WaitForCrossingState(),
										transitions={'crossing': 'crossing', 'idle': 'idle', 'failed': 'failed', 'remain': 'wait_for_crossing'},
										autonomy={'crossing': Autonomy.Off, 'idle': Autonomy.Off, 'failed': Autonomy.Off, 'remain': Autonomy.Off})

			# x:270 y:44
			OperatableStateMachine.add('check_drift_state_',
										CheckDriftState(),
										transitions={'failed': 'failed', 'drift': 'drift'},
										autonomy={'failed': Autonomy.Off, 'drift': Autonomy.Off})


		# x:30 y:365, x:130 y:365, x:230 y:365, x:330 y:365, x:430 y:365, x:530 y:365, x:630 y:365, x:730 y:365, x:830 y:365, x:930 y:365, x:1030 y:365
		_sm_navigation_1 = ConcurrencyContainer(outcomes=['failed', 'idle', 'wair_for_crossing', 'drift', 'navigation'], conditions=[
										('idle', [('Navigation', 'idle')]),
										('wair_for_crossing', [('Navigation', 'wait_for_crossing')]),
										('failed', [('Navigation', 'failed')]),
										('failed', [('check_drift', 'failed')]),
										('drift', [('check_drift', 'drift')]),
										('navigation', [('Navigation', 'remain')])
										])

		with _sm_navigation_1:
			# x:30 y:132
			OperatableStateMachine.add('Navigation',
										NavigationState(),
										transitions={'idle': 'idle', 'wait_for_crossing': 'wair_for_crossing', 'failed': 'failed', 'remain': 'navigation'},
										autonomy={'idle': Autonomy.Off, 'wait_for_crossing': Autonomy.Off, 'failed': Autonomy.Off, 'remain': Autonomy.Off})

			# x:242 y:133
			OperatableStateMachine.add('check_drift',
										CheckDriftState(),
										transitions={'failed': 'failed', 'drift': 'drift'},
										autonomy={'failed': Autonomy.Off, 'drift': Autonomy.Off})


		# x:30 y:365, x:130 y:365, x:230 y:365, x:330 y:365, x:430 y:365, x:530 y:365, x:630 y:365, x:730 y:365, x:830 y:365
		_sm_idle_2 = ConcurrencyContainer(outcomes=['navigation', 'drift', 'failed', 'idle'], conditions=[
										('navigation', [('idle_state', 'navigation')]),
										('failed', [('idle_state', 'failed')]),
										('failed', [('check_drift', 'failed')]),
										('drift', [('check_drift', 'drift')]),
										('idle', [('idle_state', 'remain')])
										])

		with _sm_idle_2:
			# x:57 y:125
			OperatableStateMachine.add('idle_state',
										IdleState(),
										transitions={'navigation': 'navigation', 'failed': 'failed', 'remain': 'idle'},
										autonomy={'navigation': Autonomy.Off, 'failed': Autonomy.Off, 'remain': Autonomy.Off})

			# x:263 y:126
			OperatableStateMachine.add('check_drift',
										CheckDriftState(),
										transitions={'failed': 'failed', 'drift': 'drift'},
										autonomy={'failed': Autonomy.Off, 'drift': Autonomy.Off})


		# x:30 y:365, x:130 y:365, x:230 y:365, x:330 y:365, x:430 y:365, x:530 y:365, x:630 y:365, x:730 y:365, x:830 y:365, x:930 y:365, x:1030 y:365
		_sm_crossing_3 = ConcurrencyContainer(outcomes=['failed', 'drift', 'navigation', 'idle', 'crossing'], conditions=[
										('navigation', [('Crossing_state', 'navigation')]),
										('failed', [('Crossing_state', 'failed')]),
										('idle', [('Crossing_state', 'idle')]),
										('drift', [('Check_drift_state', 'drift')]),
										('failed', [('Check_drift_state', 'failed')]),
										('crossing', [('Crossing_state', 'remain')])
										])

		with _sm_crossing_3:
			# x:30 y:40
			OperatableStateMachine.add('Crossing_state',
										CrossingState(),
										transitions={'idle': 'idle', 'navigation': 'navigation', 'failed': 'failed', 'remain': 'crossing'},
										autonomy={'idle': Autonomy.Off, 'navigation': Autonomy.Off, 'failed': Autonomy.Off, 'remain': Autonomy.Off})

			# x:187 y:107
			OperatableStateMachine.add('Check_drift_state',
										CheckDriftState(),
										transitions={'failed': 'failed', 'drift': 'drift'},
										autonomy={'failed': Autonomy.Off, 'drift': Autonomy.Off})



		with _state_machine:
			# x:66 y:77
			OperatableStateMachine.add('init',
										InitializationState(),
										transitions={'succeeded': 'Idle', 'failed': 'failed'},
										autonomy={'succeeded': Autonomy.Off, 'failed': Autonomy.Off})

			# x:355 y:70
			OperatableStateMachine.add('Idle',
										_sm_idle_2,
										transitions={'navigation': 'Navigation', 'drift': 'init', 'failed': 'failed', 'idle': 'Idle'},
										autonomy={'navigation': Autonomy.Inherit, 'drift': Autonomy.Inherit, 'failed': Autonomy.Inherit, 'idle': Autonomy.Inherit})

			# x:354 y:183
			OperatableStateMachine.add('Navigation',
										_sm_navigation_1,
										transitions={'failed': 'failed', 'idle': 'Idle', 'wair_for_crossing': 'Wait_for_crossing', 'drift': 'init', 'navigation': 'Navigation'},
										autonomy={'failed': Autonomy.Inherit, 'idle': Autonomy.Inherit, 'wair_for_crossing': Autonomy.Inherit, 'drift': Autonomy.Inherit, 'navigation': Autonomy.Inherit})

			# x:581 y:76
			OperatableStateMachine.add('Wait_for_crossing',
										_sm_wait_for_crossing_0,
										transitions={'crossing': 'Crossing', 'idle': 'Idle', 'failed': 'failed', 'drift': 'init', 'wait_for_crossing': 'Wait_for_crossing'},
										autonomy={'crossing': Autonomy.Inherit, 'idle': Autonomy.Inherit, 'failed': Autonomy.Inherit, 'drift': Autonomy.Inherit, 'wait_for_crossing': Autonomy.Inherit})

			# x:580 y:216
			OperatableStateMachine.add('Crossing',
										_sm_crossing_3,
										transitions={'failed': 'failed', 'drift': 'init', 'navigation': 'Navigation', 'idle': 'Idle', 'crossing': 'Crossing'},
										autonomy={'failed': Autonomy.Inherit, 'drift': Autonomy.Inherit, 'navigation': Autonomy.Inherit, 'idle': Autonomy.Inherit, 'crossing': Autonomy.Inherit})


		return _state_machine


	# Private functions can be added inside the following tags
	# [MANUAL_FUNC]
	
	# [/MANUAL_FUNC]
