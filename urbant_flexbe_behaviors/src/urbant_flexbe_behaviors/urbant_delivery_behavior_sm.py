#!/usr/bin/env python
# -*- coding: utf-8 -*-
###########################################################
#               WARNING: Generated code!                  #
#              **************************                 #
# Manual changes may get lost if file is generated again. #
# Only code inside the [MANUAL] tags will be kept.        #
###########################################################

from flexbe_core import Behavior, Autonomy, OperatableStateMachine, ConcurrencyContainer, PriorityContainer, Logger
from urbant_flexbe_states.urbant_delivery_states.delivery_arrived_state import DeliveryArrivedState
from urbant_flexbe_states.urbant_delivery_states.delivery_idle_state import DeliveryIdleState
from urbant_flexbe_states.urbant_delivery_states.delivery_state import DeliveryState
# Additional imports can be added inside the following tags
# [MANUAL_IMPORT]

# [/MANUAL_IMPORT]


'''
Created on Thu Apr 07 2022
@author: Yu-Wen Chen
'''
class urbant_delivery_behaviorSM(Behavior):
	'''
	test
	'''


	def __init__(self):
		super(urbant_delivery_behaviorSM, self).__init__()
		self.name = 'urbant_delivery_behavior'

		# parameters of this behavior
		self.add_parameter('mode', 1)
		self.add_parameter('tmp', 'tmp')
		self.add_parameter('test', 'please enter go home')
		self.add_parameter('home_node', 'N0')
		self.add_parameter('go_home', False)

		# references to used behaviors

		# Additional initialization code can be added inside the following tags
		# [MANUAL_INIT]
		
		# [/MANUAL_INIT]

		# Behavior comments:



	def create(self):
		# x:52 y:600, x:360 y:618
		_state_machine = OperatableStateMachine(outcomes=['finished', 'failed'], input_keys=['system_mode'])
		_state_machine.userdata.system_mode = self.mode
		_state_machine.userdata.tmp = self.tmp
		_state_machine.userdata.home_node = self.home_node

		# Additional creation code can be added inside the following tags
		# [MANUAL_CREATE]
		
		# [/MANUAL_CREATE]


		with _state_machine:
			# x:333 y:161
			OperatableStateMachine.add('delivery_idle',
										DeliveryIdleState(),
										transitions={'delivery': 'delivery_state', 'failed': 'failed'},
										autonomy={'delivery': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'goal_ID': 'goal_ID', 'home_ID': 'home_ID'})

			# x:550 y:164
			OperatableStateMachine.add('delivery_state',
										DeliveryState(),
										transitions={'delivered': 'arrived_state', 'delivery_failed': 'delivery_idle'},
										autonomy={'delivered': Autonomy.Off, 'delivery_failed': Autonomy.Off},
										remapping={'goal_ID': 'goal_ID'})

			# x:911 y:155
			OperatableStateMachine.add('go_home',
										DeliveryState(),
										transitions={'delivered': 'delivery_idle', 'delivery_failed': 'arrived_state'},
										autonomy={'delivered': Autonomy.Off, 'delivery_failed': Autonomy.Off},
										remapping={'goal_ID': 'goal_ID'})

			# x:728 y:156
			OperatableStateMachine.add('arrived_state',
										DeliveryArrivedState(),
										transitions={'go_home': 'go_home', 'failed': 'failed'},
										autonomy={'go_home': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'home_node': 'home_node', 'goal_ID': 'goal_ID'})


		return _state_machine


	# Private functions can be added inside the following tags
	# [MANUAL_FUNC]
	
	# [/MANUAL_FUNC]
