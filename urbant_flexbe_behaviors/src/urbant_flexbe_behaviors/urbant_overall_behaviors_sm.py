#!/usr/bin/env python
# -*- coding: utf-8 -*-
###########################################################
#               WARNING: Generated code!                  #
#              **************************                 #
# Manual changes may get lost if file is generated again. #
# Only code inside the [MANUAL] tags will be kept.        #
###########################################################

from flexbe_core import Behavior, Autonomy, OperatableStateMachine, ConcurrencyContainer, PriorityContainer, Logger
from urbant_flexbe_behaviors.urbant_delivery_behavior_sm import urbant_delivery_behaviorSM
from urbant_flexbe_states.check_drift_state import CheckDriftState
from urbant_flexbe_states.urbant_delivery_states.system_idle_state import SystemIdleState
# Additional imports can be added inside the following tags
# [MANUAL_IMPORT]

# [/MANUAL_IMPORT]


'''
Created on Wed Apr 20 2022
@author: Yu-Wen Chen
'''
class urbant_overall_behaviorsSM(Behavior):
	'''
	UrbANT overall statemachine. Begin from system_idle to either following mode or delivery mode.
	'''


	def __init__(self):
		super(urbant_overall_behaviorsSM, self).__init__()
		self.name = 'urbant_overall_behaviors'

		# parameters of this behavior
		self.add_parameter('system_mode', 1)
		self.add_parameter('home_node', 'N0')

		# references to used behaviors
		self.add_behavior(urbant_delivery_behaviorSM, 'Container/urbant_delivery_behavior')

		# Additional initialization code can be added inside the following tags
		# [MANUAL_INIT]
		
		# [/MANUAL_INIT]

		# Behavior comments:



	def create(self):
		# x:30 y:365, x:130 y:365
		_state_machine = OperatableStateMachine(outcomes=['finished', 'failed'])
		_state_machine.userdata.system_mode = self.system_mode

		# Additional creation code can be added inside the following tags
		# [MANUAL_CREATE]
		
		# [/MANUAL_CREATE]

		# x:30 y:365, x:130 y:365, x:230 y:365, x:330 y:365
		_sm_container_0 = ConcurrencyContainer(outcomes=['finished', 'failed'], conditions=[
										('finished', [('urbant_delivery_behavior', 'finished')]),
										('failed', [('urbant_delivery_behavior', 'failed')])
										])

		with _sm_container_0:
			# x:92 y:112
			OperatableStateMachine.add('urbant_delivery_behavior',
										self.use_behavior(urbant_delivery_behaviorSM, 'Container/urbant_delivery_behavior',
											default_keys=['system_mode']),
										transitions={'finished': 'finished', 'failed': 'failed'},
										autonomy={'finished': Autonomy.Inherit, 'failed': Autonomy.Inherit})

			# x:309 y:121
			OperatableStateMachine.add('check_drift',
										CheckDriftState(),
										transitions={},
										autonomy={})



		with _state_machine:
			# x:30 y:40
			OperatableStateMachine.add('system_idle',
										SystemIdleState(),
										transitions={'delivery_idle': 'Container', 'following_idle': 'finished', 'failed': 'failed'},
										autonomy={'delivery_idle': Autonomy.Off, 'following_idle': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'system_mode': 'system_mode'})

			# x:235 y:123
			OperatableStateMachine.add('Container',
										_sm_container_0,
										transitions={'finished': 'Container', 'failed': 'failed'},
										autonomy={'finished': Autonomy.Inherit, 'failed': Autonomy.Inherit})


		return _state_machine


	# Private functions can be added inside the following tags
	# [MANUAL_FUNC]
	
	# [/MANUAL_FUNC]
