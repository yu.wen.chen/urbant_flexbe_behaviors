# urbant_behaviors
This repo contains all urbant-specific states and behaviors.

# Launch FlexBe

**on UrbANT**

1. Launch the onboard behavior engine on the robot:

    ```
    roslaunch flexbe_onboard behavior_onboard.launch
    ```

2. Run the FlexBE action server:

    ```
    rosrun flexbe_widget be_action_server
    ```

3. Activate the behavior:
    
    ```
    rostopic pub /flexbe/execute_behavior/goal flexbe_msgs/BehaviorExecutionActionGoal '{goal: {behavior_name: "urbant_navgraph_behaviour"}}'
    ```

**on PC (attach GUI)**

***PC should possess the same package for behavior and node.***
* Attach the User Interface
 
    ```
    roslaunch flexbe_app flexbe_ocs.launch  
    ```

Find other methods here: http://wiki.ros.org/flexbe/Tutorials/Running%20Behaviors%20Without%20Operator

# Installation
1. Install FlexBe Engine
   
    ```
    sudo apt install ros-$ROS_DISTRO-flexbe-behavior-engine
    ```
2. Git clone FlexBe behaviors and nodes for UrbANT
   
    ```bash
    # private repo currently
    git clone https://yu.wen.chen@git.rwth-aachen.de/yu.wen.chen/urbant_flexbe_behaviors.git
    ```
3. GUI (opitonal, recommended on user PC)
    ```
    git clone https://github.com/FlexBE/flexbe_app.git
    ```

# Create new nodes for UrbANT
TODO