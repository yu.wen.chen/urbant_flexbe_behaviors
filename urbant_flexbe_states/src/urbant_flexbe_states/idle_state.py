#!/usr/bin/env python

import rospy
from flexbe_core import EventState

import tf2_ros
from tf.transformations import euler_from_quaternion
import math
import geometry_msgs.msg
from navgraph.msg import NavGraphStatus

class IdleState(EventState):
    """ Idle, waiting for command
    """
    

    def __init__(self):
        """Constructor"""
        super(IdleState, self).__init__(outcomes = ['navigation', 'failed', 'remain'])

    def execute(self, userdata):
        """Wait for action result and return outcome accordingly"""
        # Check current status
        navgraph_status = rospy.wait_for_message("/navgraph/status", NavGraphStatus)
        if (navgraph_status.state != 0):
            return 'navigation'
        else:
            return        
        

    def on_enter(self, userdata):
        """Create and send action goal"""
        pass
            
    def on_exit(self, userdata):
        pass

    def on_stop(self):
        pass