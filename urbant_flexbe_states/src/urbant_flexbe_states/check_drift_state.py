#!/usr/bin/env python

import rospy
from flexbe_core import EventState

import tf2_ros
from tf.transformations import euler_from_quaternion
import math
import geometry_msgs.msg
from navgraph.msg import NavGraphStatus
from sensor_msgs.msg import NavSatFix
import std_srvs.srv

class CheckDriftState(EventState):
    """ Idle, waiting for command
    """
    

    def __init__(self):
        """Constructor"""
        super(CheckDriftState, self).__init__()
        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer)

        self.DRIFT_TRANSLATION_THRESHOLD = 10.0 # [meter]
        self.DRIFT_ROTATION_THRESHOLD = 0.174 # [radius]
        self.GPS_COVARIANCE_THRESHOLD = 20.0 


    def execute(self, userdata):
        """Wait for action result and return outcome accordingly"""

        try:
            trans = self.tfBuffer.lookup_transform('base_link','base_link_gps', rospy.Time(), rospy.Duration(1.0))
            drift_translation = math.sqrt(trans.transform.translation.x ** 2 + trans.transform.translation.y ** 2)
            drift_rotation = abs(euler_from_quaternion([trans.transform.rotation.x, trans.transform.rotation.y, trans.transform.rotation.z, trans.transform.rotation.w])[2])
            if (drift_translation > self.DRIFT_TRANSLATION_THRESHOLD or drift_rotation > self.DRIFT_ROTATION_THRESHOLD): # if the robot drift too far from gps
                rospy.logdebug("Translational drift: %f, Rotational drift: %f, reset pose", drift_translation, drift_rotation)

                # Reset base_link to base_link_gps
                gps_msg = rospy.wait_for_message("/ublox_gps/fix", NavSatFix)

                ## check gps covariance
                if ( any(gps_cov<self.GPS_COVARIANCE_THRESHOLD for gps_cov in gps_msg.position_covariance)):
                    rospy.loginfo("Ready to set UrbANT pose.")
                    rospy.wait_for_service('set_gps_pos') # check if the service exists
                    try:
                        set_gps_pos = rospy.ServiceProxy('set_gps_pos', std_srvs.srv.Empty)
                        set_gps_pos()  # request reset pose
                    except rospy.ServiceException as e:
                        rospy.logwarn("Service call failed : %s", e)
                else:
                    rospy.logwarn("gps covariance: %f, %f, %f", gps_msg.position_covariance[0], gps_msg.position_covariance[4], gps_msg.position_covariance[8])
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException) as e:
            rospy.logwarn(str(e))
        
        # Set execution rate
        rate = rospy.Rate(5.0)
        rate.sleep()


    def on_enter(self, userdata):
        """Create and send action goal"""
        pass
            
    def on_exit(self, userdata):
        pass

    def on_stop(self):
        pass