#!/usr/bin/env python

import rospy
from flexbe_core import EventState, Logger
from flexbe_core.proxy import ProxySubscriberCached
from std_msgs.msg import Int16

class SystemIdleState(EventState):
    """ Idle, waiting for command
    """
    

    def __init__(self):
        """Constructor"""
        super(SystemIdleState, self).__init__(outcomes = ['delivery_idle', 'following_idle', 'failed'], input_keys = ['system_mode'])

    def execute(self, userdata):
        """Wait for action result and return outcome accordingly"""

        # Check system mode: 
        # 1. delivery
        # 2. following
        
        system_mode = userdata.system_mode
        Logger.loginfo("Enter mode %i", system_mode)
        if (system_mode == 1):
            Logger.loginfo("Enter delivery mode")
            return 'delivery_idle'
        elif (system_mode == 2):
            Logger.loginfo("Enter following mode")
            return 'following_idle'

    def on_enter(self, userdata):
        """Create and send action goal"""
        pass
            
    def on_exit(self, userdata):
        pass

    def on_stop(self):
        pass