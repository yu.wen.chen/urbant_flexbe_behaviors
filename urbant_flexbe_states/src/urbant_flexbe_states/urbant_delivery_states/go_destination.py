#!/usr/bin/env python

import rospy
from flexbe_core import EventState

from navgraph.msg import NavGraphStatus
from navgraph.srv import navgraphDriveTo

class GoDestinationState(EventState):
    """ Idle, waiting for command
    """
    

    def __init__(self):
        """Constructor"""
        super(GoDestinationState, self).__init__(outcomes = ['success', 'failed'])

    def execute(self, userdata):
        """Wait for action result and return outcome accordingly"""
        navigation_status = rospy.wait_for_message("/navgraph/status", NavGraphStatus) 


        # Check current status
        if (navigation_status.state == 0):
            return 'success'

    def on_enter(self, userdata):
        """Create and send action goal"""
        rospy.loginfo("Go Destination")

        # in case returning from crossing by Flexbe, send service request to continue navigation
        rospy.wait_for_service('navgraph/drive_to')
        try:
            navgraph_handle = rospy.ServiceProxy('navgraph/drive_to', navgraphDriveTo)
            navgraph_handle('N1')
            rospy.sleep(2)
        except rospy.ServiceException as e:
            rospy.logwarn("Service call failed %s", str(e))
            return 'failed'
        

    def on_exit(self, userdata):
        pass

    def on_stop(self):
        pass