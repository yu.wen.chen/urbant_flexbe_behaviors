#!/usr/bin/env python

from sre_constants import SUCCESS
import rospy
from flexbe_core import EventState, Logger
from std_msgs.msg import Bool, String
from flexbe_msgs.msg import BehaviorInputAction, BehaviorInputGoal, BehaviorInputResult
from flexbe_core.proxy import ProxyActionClient
from std_srvs.srv import Trigger, TriggerResponse


class DeliveryArrivedState(EventState):
    """ Idle, waiting for command
    """

    def __init__(self):
        """Constructor"""
        super(DeliveryArrivedState, self).__init__(outcomes = ['go_home', 'failed'], input_keys=['home_node'], output_keys = ['goal_ID'])
        self.go_home_trigger_ = False
        
        # self._go_home_topic = '/delivery/go_home'

    def execute(self, userdata):
        """Wait for action result and return outcome accordingly"""

        # Logger.loginfo("Waiting for trigger to go home")
        # goal = rospy.wait_for_message(self._go_home_topic, Bool)
        # userdata.goal_ID = String('N0')
        if self.go_home_trigger_:
            userdata.goal_ID = userdata.home_node
            self.go_home_trigger_ = False
            return 'go_home'

    def trigger_response(self, request):
        self.go_home_trigger_ = True
        return TriggerResponse(
            success=True
            )

    def on_enter(self, userdata):
        """Create and send action goal"""
        self.go_home_service_ = rospy.Service('delivery/go_home', Trigger, self.trigger_response)
        pass
            
    def on_exit(self, userdata):
        self.go_home_service_.shutdown()
        pass

    def on_stop(self):
        pass