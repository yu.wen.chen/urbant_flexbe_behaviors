#!/usr/bin/env python

from tkinter import W
import rospy
from flexbe_core import EventState, Logger
from flexbe_core.proxy import ProxyActionClient
from std_msgs.msg import String
from actionlib import SimpleActionClient, TerminalState
from navgraph.msg import navgraphDTAction, navgraphDTActionGoal
from navgraph.msg import NavGraphStatus



class DeliveryState(EventState):
    """ Idle, waiting for command
    """
    

    def __init__(self):
        """Constructor"""
        super(DeliveryState, self).__init__(outcomes = ['delivered', 'delivery_failed'], input_keys = ['goal_ID'])

        self._action_topic = "/navgraph/drive_to"
        self._failed = False

    def execute(self, userdata):
        # Check if the client failed to send the goal 
        if self._failed:
            return 'delivery_failed'

        # Check if the action has ben finished
        if self._client.has_result(self._action_topic):
            return 'delivered'
        else: 
            if self._client.has_feedback(self._action_topic):
                action_feedback = NavGraphStatus()
                action_feedback = self._client.get_feedback(self._action_topic)
                Logger.loginfo("Current goal: %s, Navgraph state:%s", action_feedback.status.current_node,str(action_feedback.status.state))

    def on_enter(self, userdata):
        """Create and send action goal"""
        # Set the goal
        goal = navgraphDTActionGoal()
        goal = String(userdata.goal_ID)
        self._client = ProxyActionClient({self._action_topic:navgraphDTAction})

        # Send the goal
        self._failed = False

        try:
            self._client.send_goal(self._action_topic, goal)
        except Exception as e:
            Logger.logwarn('Failed to start the delivery. \n%s', str(e))
            self._error = True

            
    def on_exit(self, userdata):
        if not self._client.has_result(self._action_topic):
            self._client.cancel(self._action_topic)
            Logger.loginfo('Cancelled active action goal.')

    def on_stop(self):
        pass