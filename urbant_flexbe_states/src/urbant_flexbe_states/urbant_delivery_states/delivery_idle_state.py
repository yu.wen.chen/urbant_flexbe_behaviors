#!/usr/bin/env python

import rospy
from flexbe_core import EventState, Logger
from std_msgs.msg import String
from navgraph.srv import navgraphDriveTo

class DeliveryIdleState(EventState):
    """ Idle, waiting for command
    """
    

    def __init__(self):
        """Constructor"""
        super(DeliveryIdleState, self).__init__(outcomes = ['delivery', 'failed'], output_keys = ['goal_ID', 'home_ID'])

        self.goal_set_ = False
        self.delivery_goal_ = ''

    def execute(self, userdata):
        """Wait for action result and return outcome accordingly"""

        Logger.loginfo("Waiting for delivery address")

        if (self.goal_set_):
            userdata.goal_ID = self.delivery_goal_
            Logger.loginfo("current goal %s", userdata.goal_ID)
            self.goal_set_ = False
            return 'delivery'

    def trigger_response(self, request):
        # TODO: check if goal valid
        self.goal_set_ = True
        self.delivery_goal_ = request.data

    def on_enter(self, userdata):
        """Create and send action goal"""
        self.set_goal_service_ = rospy.Service('delivery/goal', navgraphDriveTo, self.trigger_response)
        pass
            
    def on_exit(self, userdata):
        self.set_goal_service_.shutdown()
        pass

    def on_stop(self):
        pass