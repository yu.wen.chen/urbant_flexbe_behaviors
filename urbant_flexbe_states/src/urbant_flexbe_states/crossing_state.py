#!/usr/bin/env python

import rospy
from flexbe_core import EventState

import tf2_ros
from tf.transformations import euler_from_quaternion
import math
import geometry_msgs.msg
from navgraph.msg import NavGraphStatus
from navgraph.srv import navgraphHandle

class CrossingState(EventState):
    """ Idle, waiting for command
    """
    

    def __init__(self):
        """Constructor"""
        super(CrossingState, self).__init__(outcomes = ['idle', 'navigation', 'failed', 'remain'])

    def execute(self, userdata):
        """Wait for action result and return outcome accordingly"""

        # Check current status
        navgraph_status = rospy.wait_for_message("/navgraph/status", NavGraphStatus)
        if (navgraph_status.state == 0):
            return 'idle'
        elif (navgraph_status.state == 1):
            return 'navigation'
        else:
            return 
    
        

    def on_enter(self, userdata):
        """Create and send action goal"""
        rospy.loginfo("Start navigation")

        # in case returning from crossing by Flexbe, send service request to continue navigation
        rospy.wait_for_service('navgraph/handle')
        try:
            navgraph_handle = rospy.ServiceProxy('navgraph/handle', navgraphHandle)
            navgraph_handle(1)
        except rospy.ServiceException as e:
            rospy.logwarn("Service call failed %s", str(e))
        
        rospy.loginfo("test")

            
    def on_exit(self, userdata):
        pass

    def on_stop(self):
        pass