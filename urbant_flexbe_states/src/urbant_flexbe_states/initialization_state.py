#!/usr/bin/env python

import rospy
from flexbe_core import EventState

from sensor_msgs.msg import NavSatFix
from sensor_msgs.msg import Imu

import std_srvs.srv

class InitializationState(EventState):
    """Initialization for Localization and Navigation
    """
    

    def __init__(self):
        """Constructor"""
        super(InitializationState, self).__init__(outcomes = ['succeeded', 'failed'])


    def execute(self, userdata):
        """Wait for action result and return outcome accordingly"""

        gps_msg = rospy.wait_for_message("/ublox_gps/fix", NavSatFix)
        imu_msg = rospy.wait_for_message("/imu/data", Imu)

        if ( any(gps_cov>35.0 for gps_cov in gps_msg.position_covariance)):
            rospy.loginfo("gps covariance: %f, %f, %f", gps_msg.position_covariance[0], gps_msg.position_covariance[4], gps_msg.position_covariance[8])
        else:
            rospy.loginfo("Ready to set UrbANT pose.")
            rospy.wait_for_service('set_gps_pos') # check if the service exists
            try:
                set_gps_pos = rospy.ServiceProxy('set_gps_pos', std_srvs.srv.Empty)
                set_gps_pos()
                return 'succeeded'
            except rospy.ServiceException as e:
                rospy.logwarn("Service call failed : %s", e)
                return 'failed'


    def on_enter(self, userdata):
        pass

    def on_exit(self, userdata):
        pass

    def on_stop(self):
        pass